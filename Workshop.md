# Introduction to Git & Bitbucket

The source for this workshop is available as a Git repository at [bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop](https://bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop).

As you go through this workshop you will find it useful to write down your observations as to how you would improve it, either for yourself or others, since you will be asked to contribute some changes back to this document.

## Introducing Git

### Getting Git

* Download Git for your operating system (if it's not already installed) from [git-scm.com](http://git-scm.com/).
* Install Git using the downloaded installer. Accept the default options.

### Getting Your Environment Ready

#### Windows

* Create a folder you want to use for your code repository.
* Right click the folder and choose "Git Bash" from the menu.

#### Everyone Else

* Create a folder you want to use for your code repository.
* Open a terminal, and `cd` to the directory you wish to use for your code repository.

### Creating & Initial Commit

* Create your first repository by running the `git init` command.
* Create or copy some files into your repository folder --- referred to from now on as your "working directory". This can be anything from a simple 'hello world' script through to an entire piece of code.
* Run the `git add .` command to add the files to your repository.
* Run `git commit -a -m 'First commit.'` to commit your changes to your repository. You may be prompted to set your email address. If you are, use your lincoln.ac.uk address to properly attribute your commits later on.
* Type `git status`.

If everything has gone to plan you should see

	# On branch master
	nothing to commit, working directory clean
	
The command `git status` is useful at all points in development, since it shows you files you've not yet added and changes which haven't yet been committed.

### Make Some Changes

* Change one or two of your files, or add and remove files. Don't forget that if you're adding new files you will need to run `git add .` again.
* Run `git commit -a -m` to commit your changes, leaving a suitable message after the `-m`.

You can -- and should -- repeat the `git add` and `git commit` steps whenever you feel your code has reached a point worth recording. It is not unusual in software development companies to commit hundreds of times a day, and with Git there is very little overhead in terms of time or storage to committing, so do it often!

* Make two or three more changes and commits to your code.

If you want to see a complete list of your commits you can do this with the `git log` command.

### Time Travelling (or; Returning to a Previous Commit)

One thing you will want to do with Git is return to a previous commit, most commonly to return to a prior copy of working code. Git actually has three ways of doing this, depending on what you're trying to achieve.

#### Checkout

If you want to just look at an old copy of code you can use the `git checkout` command to return to a specific commit. Run `git log` to see a list of commit identifiers (a string of letters and numbers) along with times and commit messages. Find the identifier of the commit you want, and then run `git checkout [identifier]`, for example for identifier "0766c053..." the command would be `git checkout 0766c053...`.

* Run the `git checkout` command using your first commit.

Running this command will change the state of your working directory to match how it was at the time of the commit.  From here you can can look around to see how things were in the past and make experimental commits, but your changes will *not* be kept permanently once you run another `git checkout`. If you want to, you can use the `-b` flag when you run `checkout` to create a new branch of your code and keep subsequent changes permanently.

* To return to the latest copy of your code run the command `git checkout master`. This returns you to the "master" branch, and the latest commit.

#### Revert

The `git revert` operation undoes changes made in a specific commit, creating a second commit which effectively performs the reverse of the original.

* Use `git log` to find a commit you wish to revert, then run `git revert [identifier]`.

#### Reset

To permanently undo changes you can use the `git reset` command. This effectively rewrites history by returning the entire repository, not just your working directory, to a previous point in the commit history. Any commits you make from now on will act as though previous commits were not made. You should be careful when using `git reset`, especially if you have already published changes made.

To reset your repository to an earlier point, run `git log` to find a commit identifier, then run `git reset --hard [identifier]`.

## Introducing Bitbucket

[Bitbucket](https://bitbucket.org/) is our recommended external Git host, since they provide a free academic tier which gives you unlimited public and private Git repositories. For the rest of this workshop you will need a Bitbucket account, and we recommend that you continue using Bitbucket to keep your code safe in future.

* Visit [bitbucket.org/account/signup](https://bitbucket.org/account/signup/) to register for your free account if you haven't already. Choose an "individual" account, and be sure to use your lincoln.ac.uk email address. If you want to then you can change your email later, but you need to use your University address to earn the free academic account.
* Confirm your email address.

At the beginning of this workshop you were encouraged to note observations and changes you would make to this workshop to help yourself or others in future. You will now make a copy of the original of this workshop held on Bitbucket, make your changes, and contribute them back. The processes involved are called forking, cloning, and pull requests.

### Forking

Forking is the action of taking an existing repository and making a copy of it for your own purposes. A common example is in Open Source software, where people will fork a project to make their own changes even if they don't have permission to write to the original repository.

Forking isn't an inherent part of Git, but instead is commonly implemented by external Git hosts such as Bitbucket or GitHub. Forking is made possible (and easy) by Git being a distributed source control system, where any person with access to a copy of the code can choose to take development in their own direction.

* Visit [bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop](https://bitbucket.org/jacksonj04/introduction-to-git-bitbucket-workshop) and click the "fork" button.
* You can change any of the information in the form if you want to, but if you're happy with the defaults just click the "Fork repository" button.
* Wait a moment while the repository is forked.

### Cloning

Now you have forked the repository you have your own copy of this workshop, with which you can do whatever you like.

## Your Code

## Useful Resources

* If you want to do some more reading on Git, including advanced branching techniques, the entirety of Pro Git is available online for free at [git-scm.com/book](http://git-scm.com/book).
* Stack Overflow is full of questions and answers about Git, which you can find (or ask your own) at [hstackoverflow.com/questions/tagged/git](http://stackoverflow.com/questions/tagged/git).
* This workshop document is written using Markdown. More information on the syntax is available at [daringfireball.net/projects/markdown](http://daringfireball.net/projects/markdown/).